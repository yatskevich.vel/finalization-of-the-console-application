# MyProfile app

SEPARATOR = '------------------------------------------'

# user profile
name = ''
age = 0
phone = ''
email = ''
postcode = ''
mailing_address = ''
info = ''
# social links
psrnsp = '' #оргнип
tin = ''  #инн
current_account = '' #расчетный счет
bank_name = ''
bic = '' #v
сorresponding_account = '' #корреспонденский счет


def general_info_user(n_parameter, a_parameter, ph_parameter, e_parameter, p_parameter, m_parameter, i_parameter):
    print(SEPARATOR)
    print('Имя:    ', n_parameter)
    if 11 <= a_parameter % 100 <= 19: years_parameter = 'лет'
    elif a_parameter % 10 == 1: years_parameter = 'год'
    elif 2 <= a_parameter % 10 <= 4: years_parameter = 'года'
    else: years_parameter = 'лет'


    print('Возраст:', a_parameter, years_parameter)
    print('Телефон:', ph_parameter)
    print('E-mail:', e_parameter)
    print('Индекс:', p_parameter)
    print('Почтовый адрес:', m_parameter)
    if info:
            print('')
            print('Дополнительная информация:')
            print(i_parameter)
    

print('Приложение MyProfile')
print('Сохраняй информацию о себе и выводи ее в разных форматах')

while True:
    # main menu
    print(SEPARATOR)
    print('ГЛАВНОЕ МЕНЮ')
    print('1 - Ввести или обновить информацию')
    print('2 - Вывести информацию')
    print('0 - Завершить работу')

    option = int(input('Введите номер пункта меню: '))
    if option == 0:
            break

    if option == 1:
        # submenu 1: edit info
        while True:
            print(SEPARATOR)
            print('ВВЕСТИ ИЛИ ОБНОВИТЬ ИНФОРМАЦИЮ')
            print('1 - Личная информация')
            print('2 - Информация о предпринимателе')
            print('0 - Назад')

            option2 = int(input('Введите номер пункта меню: '))
            if option2 == 0:
                break
            if option2 == 1:
                # input general info
                name = input('Введите имя: ')
                while 1:
                        # validate user age
                        age = int(input('Введите возраст: '))
                        if age > 0:
                            break
                        print('Возраст должен быть положительным')

                phone = input('Введите номер телефона (+375ХХХХХХХХX): ')
                ph = ''
                for ch in phone:
                    if ch == '+' or ('0' <= ch <= '9'):
                        ph += ch
                phone = ph

                email = input('Введите адрес электронной почты: ')
                postcode = input('Введите потовый индекс:')
                p = ''
                for i in postcode:
                    if ('0' <= i <= '9'):
                        p += i
                postcode = p    
                mailing_address = input('Введите почтовый адрес (без индекса:) - ')
                info = input('Введите дополнительную информацию:\n')

            elif option2 == 2:
                # input social links
                while 1:
                    psrnsp = int(input('Введите ОГРНИП: '))
                    count = 0
                    psr = psrnsp
                    while psr > 0:
                        psr //= 10
                        count += 1
                    if count == 15:
                        break
                    print('ОГРНИП должен состоять из 15 цифр')
                    
                tin = int(input('Введите ИНН: '))  #инн

                while 1:
                    current_account = int(input('Введите расчетный счет: '))
                    count = 0
                    cur = current_account
                    while cur > 0:
                        cur //= 10
                        count +=1
                    if count == 20:
                        break
                    print('Расчетный счет должен состоять из 20 цифр')
                                        
                bank_name = input('Введите название банка: ')
                bic = input('Введите БИК: ')
                сorresponding_account = input('Введите корреспонденский счет: ')
            else: print('Введите корректный пункт меню')
    elif option == 2:
        # submenu 2: print info
        while True:
            print(SEPARATOR)
            print('ВЫВЕСТИ ИНФОРМАЦИЮ')
            print('1 - Личная информация')
            print('2 - Вся информация')
            print('0 - Назад')

            option2 = int(input('Введите номер пункта меню: '))
            if option2 == 0:
                break
            if option2 == 1:
                general_info_user(name, age, phone, email, postcode, mailing_address, info)

            elif option2 == 2:
                general_info_user(name, age, phone, email, postcode, mailing_address, info)

                # print social links
                print('')
                print('Информация о предпренимателе')
                print('ОГРНИП:', psrnsp)
                print('ИНН:', tin)
                print('Банковские реквизиты')
                print('Р/с:', current_account)
                print('Банк:', bank_name)
                print('БИК:', bic)
                print('К/с', сorresponding_account)
            else:   print('Введите корректный пункт меню')
    else:       print('Введите корректный пункт меню')
